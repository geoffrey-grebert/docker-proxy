FROM node:7

WORKDIR app
ADD package.json package.json
RUN npm install --prod

ADD lib lib

ENTRYPOINT ["node", "--harmony", "lib/index"]

