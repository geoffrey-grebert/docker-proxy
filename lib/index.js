const yaml = require('yamljs')
const http = require('http')
const httpProxy = require('http-proxy')

let config = process.argv[2]
config = yaml.parse(config)

const proxy = httpProxy.createProxyServer({})
proxy.on('error', function (err, req, res) {
  res.write('Something went wrong.')
  res.end()
})

const server = http.createServer(function(req, res) {
	try {
	  const routes = config.routes.filter(route => req.url.startsWith(route.path))
	  const route = routes[routes.length - 1]
	  proxy.web(req, res, { target: route.target })
	} catch(err) {
		console.log(err)
		res.send(500, err)
	}
})

if (config.websockets) {
	server.on('upgrade', function (req, socket, head) {
	  proxy.ws(req, socket, head, { target: config.websockets })
	})
}

console.log(`listening on port ${process.env.PORT || 80}`)
server.listen(process.env.PORT || 80)